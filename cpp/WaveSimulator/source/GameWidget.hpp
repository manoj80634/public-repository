/*
 * copyright (c) 2016 anton segerkvist
 * all rights reserved
 */

#ifndef GameWidget_hpp
#define GameWidget_hpp

#include <QDebug>
#include <QGLWidget>
#include <QKeyEvent>
#include <QTimer>
#include <QWidget>
#include <QVector3D>
#include <QMatrix4x4>
#include "WavePlane.hpp"

class GameWidget : public QGLWidget
{
  Q_OBJECT

public:

  GameWidget(QWidget* parent=0);

  virtual ~GameWidget();

  inline QSize sizeHint() const
  { return QSize(640, 480); }

  void initializeGL();

  void resizeGL(int w, int h);

  void paintGL();

  void drawPlane();

public slots:

  void calculate();

protected:

  void keyPressEvent(QKeyEvent* event);

private:

  QTimer m_timer;
  WavePlane m_wavePlane;

  QVector3D m_camera; // in cylindrical coordinates.

};

#endif // GameWidget_hpp
