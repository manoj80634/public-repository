/*
 * copyright (c) 2016 anton segerkvist
 * all rights reserved
 */

#include <iostream>
#include <QApplication>
#include "MainWindow.hpp"
#include "WavePlane.hpp"

int main(int argc, char** argv)
{
	QApplication app(argc, argv);

	MainWindow window;
	window.show();

	return app.exec();
}
